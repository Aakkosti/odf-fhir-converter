package kaisanlahti.odffhirconverter;

import kaisanlahti.odffhirconverter.converter.ResponseHandler;
import kaisanlahti.odffhirconverter.converter.cmdlineUI;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        if (args.length != 3) {
            System.err.println("Provide O-MI Patient URL (e.g. http://localhost:8080/Objects/Patient1) as arg 1, desired" +
                    "converter port as arg 2 and FHIR server url as arg 3.");
            System.exit(-1);
        }
        Pattern p = Pattern.compile("(.*)\\/Objects\\/(.*)");
        Matcher m = p.matcher(args[0]);
        if (!m.find()) {
            System.err.println("Malformed O-MI url. Example of the correct format: http://www.example.com/Objects/PatientId");
        }
        String omiUrl = m.group(1);
        String patientId = m.group(2);
        String fhirUrl = args[2];
        ConnectionHelper.setFhirServer(fhirUrl);
        ConnectionHelper.setOmiServer(omiUrl);
        int portNumber = Integer.parseInt(args[1]);
        new ResponseHandler(portNumber).start();
        new cmdlineUI(patientId, args[1]).startUI();
    }
}
