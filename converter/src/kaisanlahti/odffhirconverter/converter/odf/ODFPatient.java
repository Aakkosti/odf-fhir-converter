package kaisanlahti.odffhirconverter.converter.odf;

import org.jdom2.Attribute;
import org.jdom2.Element;
import org.jdom2.filter.ElementFilter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by jhkaisan on 2016/08/19.
 */
public class ODFPatient extends ODFObject {

    private final String id;
    private final String givenName;
    private final String familyName;
    private final Date birthDate;
    private final String gender;
    private final ArrayList<ODFMeasurement> odfMeasurements = new ArrayList<>();

    public ODFPatient(Element parentElement) {
        String tempGivenName = "";
        String tempFamilyName = "";
        String tempGender = "";
        Date tempDate = null;
        Iterator<Element> infoItems = parentElement.getDescendants(new ElementFilter("InfoItem"));
        while (infoItems.hasNext()) {
            Element e = infoItems.next();
            Attribute nameAttr = e.getAttribute("name");
            if (nameAttr.getValue().equals("birth-date")) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    tempDate = format.parse(e.getContent(0).getValue());
                } catch (ParseException exception) {
                    System.err.println("Can't parse date from ODF. Date string: " + e.getContent(0).getValue());
                    exception.printStackTrace();
                }
            } else if (nameAttr.getValue().equals("first-name")) {
                tempGivenName = e.getContent(0).getValue();
            } else if (nameAttr.getValue().equals("last-name")) {
                tempFamilyName = e.getContent(0).getValue();
            } else if (nameAttr.getValue().equals("gender")) {
                tempGender = e.getContent(0).getValue();
            }
        }
        this.id = parseId(parentElement);
        odfMeasurements.addAll(getMeasurements(parentElement));
        this.givenName = tempGivenName;
        this.familyName = tempFamilyName;
        this.birthDate = tempDate;
        this.gender = tempGender;
    }

    private ArrayList<ODFMeasurement> getMeasurements(Element parentElement) {
        ArrayList<ODFMeasurement> measurements = new ArrayList<>();
        Iterator<Element> objectIterator = parentElement.getDescendants(new ElementFilter("Object"));
        while (objectIterator.hasNext()) {
            Element objectElement = objectIterator.next();
            measurements.add(new ODFMeasurement(objectElement, this.id));
        }
        return measurements;
    }

    public ArrayList<ODFMeasurement> getOdfMeasurements() {
        return odfMeasurements;
    }

    public String getId() {
        return id;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public String getGender() {
        return gender;
    }

    public String getGivenName() {
        return givenName;
    }

    public String getFamilyName() {
        return familyName;
    }
}
