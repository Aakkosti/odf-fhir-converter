package kaisanlahti.odffhirconverter.converter.odf;

import kaisanlahti.odffhirconverter.ConnectionHelper;
import kaisanlahti.odffhirconverter.converter.fhir.FHIRMessageFactory;
import kaisanlahti.odffhirconverter.converter.fhir.FHIRParser;
import kaisanlahti.odffhirconverter.converter.omi.OMIMessageFactory;
import kaisanlahti.odffhirconverter.converter.omi.OMIParser;
import org.jdom2.Element;
import org.jdom2.filter.ElementFilter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jhkaisan on 2016/08/19.
 */
public class ODFMeasurement extends ODFObject {

    private final String id;
    private final String deviceId;
    private final String unit;
    private final String value;
    private final Date timestamp;
    private final String patientId;

    public ODFMeasurement(Element parentElement, String patientId) {
        String tempUrl = "";
        String tempUnit = "";
        String tempValue = "";
        Date tempDate = null;
        this.patientId = patientId;

        Iterator<Element> infoItemIterator = parentElement.getDescendants(new ElementFilter("InfoItem"));
        while (infoItemIterator.hasNext()) {
            Element infoItem = infoItemIterator.next();
            String name = infoItem.getAttributeValue("name");
            Element valueElement = infoItem.getContent(new ElementFilter("value")).get(0);
            String value = valueElement.getText();
            switch (name) {
                case "device":
                    tempUrl = value;
                    break;
                case "measurement":
                    tempValue = value;
                    tempDate = new Date(Long.parseLong(valueElement.getAttributeValue("unixTime"))*1000);
                    break;
            }
        }
        this.id = parseId(parentElement);
        if (tempUrl != null) {
            tempUrl = getDeviceFromServer();
        }
        this.deviceId = getFhirDeviceId(tempUrl);
        this.value = tempValue;
        this.timestamp = tempDate;
        this.unit = getUnitFromServer();

    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getPatientId() {
        return patientId;
    }

    private String getDeviceFromServer() {
        OMIParser omiParser = new OMIParser();
        ConnectionHelper connectionHelper = new ConnectionHelper();
        ArrayList<String> objectIds = new ArrayList<>();
        objectIds.add(patientId);
        objectIds.add(id);
        String message = OMIMessageFactory.getOMIReadObjectMessage(objectIds, "device");
        String response = connectionHelper.postOmiMessage(message);
        return omiParser.getValueFromMessage(response);

    }

    private String getFhirDeviceId(String odfDeviceId) {
        FHIRParser fhirParser = new FHIRParser();
        ConnectionHelper connectionHelper = new ConnectionHelper();
        if (odfDeviceId != null) {
            Pattern p = Pattern.compile("(.*)\\/Objects\\/(.*)");
            Matcher m = p.matcher(odfDeviceId);
            if (m.find()) {
                String deviceId = m.group(2);
                String getFhirDeviceResponse = connectionHelper.getFhir("Device", deviceId);
                String fhirDeviceId;
                if (getFhirDeviceResponse == null) {
                    String postResponse = postDeviceToFhir(deviceId);
                }
                fhirDeviceId = fhirParser.parseResourceIdFromSearch("Device", getFhirDeviceResponse);
                return fhirDeviceId;
            }
        }
        return null;
        }

    private String postDeviceToFhir(String deviceId) {
        ConnectionHelper connectionHelper = new ConnectionHelper();
        OMIParser omiParser = new OMIParser();
        String getDeviceMessage = OMIMessageFactory.getOMIReadObjectMessage(deviceId);
        String getDeviceResponse = connectionHelper.postOmiMessage(getDeviceMessage);
        ODFDevice device = omiParser.parseODFDevice(getDeviceResponse);
        String fhirPostDeviceMessage = FHIRMessageFactory.getFHIRDevice(device);
        String fhirPostResponse = connectionHelper.postFhirMessage(fhirPostDeviceMessage);
        return new FHIRParser().parseResourceIdFromSearch("Device", fhirPostResponse);
    }

    public String getDeviceId() {
        return deviceId;
    }

    private String getUnitFromServer() {
        ConnectionHelper helper = new ConnectionHelper();
        ArrayList<String> objectIds = new ArrayList<>();
        objectIds.add(this.patientId);
        objectIds.add(this.id);
        String getUnitMessage = OMIMessageFactory.getMetadataForInfoItem(objectIds, "measurement");
        String response = helper.postOmiMessage(getUnitMessage);
        return new OMIParser().parseUnitFromMetadata(response);
    }

    public String getUnit() {
        return unit;
    }
}
