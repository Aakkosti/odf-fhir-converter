package kaisanlahti.odffhirconverter.converter.odf;

import org.jdom2.Element;
import org.jdom2.filter.ElementFilter;

import java.util.Iterator;

/**
 * Created by Jaakko on 20.8.2016.
 */
public class ODFDevice extends ODFObject {

    private final String manufacturer;
    private final String model;
    private final String id;
    private final String serialNumber;

    public ODFDevice(Element odfObjectElement) {
        String tempManufacturer = null;
        String tempModel = null;
        String tempSerialNumber = null;
        Iterator<Element> infoItemIterator = odfObjectElement.getDescendants(new ElementFilter("InfoItem"));
        while (infoItemIterator.hasNext()) {
            Element infoItem = infoItemIterator.next();
            if (infoItem.getAttributeValue("name").equals("Manufacturer")) {
                Element value = infoItem.getContent(new ElementFilter("value")).get(0);
                tempManufacturer = value.getText();
            } else if (infoItem.getAttributeValue("name").equals("Model")) {
                Element value = infoItem.getContent(new ElementFilter("value")).get(0);
                tempModel = value.getText();
            }
            else if (infoItem.getAttributeValue("name").equals("Serial number")) {
                Element value = infoItem.getContent(new ElementFilter("value")).get(0);
                tempSerialNumber = value.getText();
            }
        }
        this.id = parseId(odfObjectElement);
        this.manufacturer = tempManufacturer;
        this.model = tempModel;
        this.serialNumber = tempSerialNumber;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public String getId() {
        return id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }
}
