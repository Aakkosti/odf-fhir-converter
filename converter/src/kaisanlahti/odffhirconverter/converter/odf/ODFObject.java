package kaisanlahti.odffhirconverter.converter.odf;

import org.jdom2.Element;
import org.jdom2.filter.ElementFilter;

import java.util.Iterator;

/**
 * Created by jhkaisan on 2016/08/19.
 */
public abstract class ODFObject {
    protected String parseId(Element e) {
        Iterator<Element> ids = e.getDescendants(new ElementFilter("id"));
        while (ids.hasNext()) {
            Element idElement = ids.next();
            return idElement.getValue();
        }
        return "";
    }
}
