package kaisanlahti.odffhirconverter.converter;

import kaisanlahti.odffhirconverter.converter.fhir.FHIRConverterThread;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Created by jhkaisan on 2016/08/13.
 */
public class ResponseHandler extends Thread {

    private final int portNumber;

    public ResponseHandler(int portNumber) {
        super("ResponseHandler");
        this.portNumber = portNumber;
    }

    public void run() {
        boolean listening = true;
        try (ServerSocket serverSocket = new ServerSocket(portNumber)) {
            while (listening) {
                new FHIRConverterThread(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            System.err.println("Could not listen on port " + portNumber);
            System.exit(-1);
        }
    }
}
