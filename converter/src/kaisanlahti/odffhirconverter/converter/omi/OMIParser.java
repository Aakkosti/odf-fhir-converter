package kaisanlahti.odffhirconverter.converter.omi;

import kaisanlahti.odffhirconverter.converter.odf.ODFDevice;
import kaisanlahti.odffhirconverter.converter.odf.ODFPatient;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.filter.ElementFilter;
import org.jdom2.input.SAXBuilder;


import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jhkaisan on 2016/08/13.
 */
public class OMIParser {

    private final SAXBuilder saxBuilder = new SAXBuilder();

    public ODFPatient parsePatientObjectResponse(String response) {
        Document document;
        try {
            document = saxBuilder.build(new StringReader(response));
            ElementFilter filter = new ElementFilter("Object");
            Iterator<Element> objectElementIterator = document.getDescendants(filter);
            while (objectElementIterator.hasNext()) {
                Element objectElement = objectElementIterator.next();
                return new ODFPatient(objectElement);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ODFDevice parseODFDevice(String response) {
        Document document;
        try {
            document = saxBuilder.build(new StringReader(response));
            Iterator<Element> objectIterator = document.getDescendants(new ElementFilter("Object"));
            while (objectIterator.hasNext()) {
                Element e = objectIterator.next();
                return new ODFDevice(e);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public int parseSubscriptionResponse(String response) {
        String literal = "<omi:requestID>([0-9]*)<\\/omi:requestID>";
        Pattern pattern = Pattern.compile(literal);
        Matcher matcher = pattern.matcher(response);
        ArrayList<String> objectIds = new ArrayList<String>();
        while (matcher.find()) {
            objectIds.add(matcher.group(1));
        }
        return Integer.parseInt(objectIds.get(0));
    }

    public ODFPatient parseSubscriptionMessage(String response) {
        Document document;
        ODFPatient patient = null;
        try {
            document = saxBuilder.build(new StringReader(response));
            ElementFilter filter = new ElementFilter("Objects");
            Element objectsElement = document.getDescendants(filter).next();
            List<Element> objectsList = objectsElement.getChildren();
            for (Element e : objectsList) {
                patient = new ODFPatient(e);
                String foo = "bar";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  patient;
    }

    public String getValueFromMessage(String message) {
        String url = null;
        try {
            Document document = saxBuilder.build(new StringReader(message));
            Iterator<Element> valueIterator = document.getDescendants(new ElementFilter("value"));
            while (valueIterator.hasNext()) {
                Element value = valueIterator.next();
                url = value.getText();
                break;
            }
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return url;
    }

    public String parseUnitFromMetadata(String message) {
        try {
            Document document = saxBuilder.build(new StringReader(message));
            Iterator<Element> valueIterator = document.getDescendants(new ElementFilter("value"));
            while (valueIterator.hasNext()) {
                return valueIterator.next().getText();
            }
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
