package kaisanlahti.odffhirconverter.converter.omi;

/**
 * Created by jhkaisan on 2016/08/13.
 */
public class OMISubscriptionData {

    private final String patient;
    private final String infoItem;
    private Integer requestId;

    public OMISubscriptionData(String patient, String infoItem) {
        this.infoItem = infoItem;
        this.patient = patient;
    }

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public String getInfoItem() {
        return infoItem;
    }

    public String getPatient() {
        return patient;
    }
}
