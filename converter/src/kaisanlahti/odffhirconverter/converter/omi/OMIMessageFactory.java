package kaisanlahti.odffhirconverter.converter.omi;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jhkaisan on 2016/08/13.
 */
public class OMIMessageFactory {

    private static final XMLOutputter XML_OUTPUTTER = getPrettyOutputter();

    private static XMLOutputter getPrettyOutputter() {
        XMLOutputter outputter = new XMLOutputter();
        outputter.setFormat(Format.getPrettyFormat());
        return outputter;
    }

    /**
     * Convenience method for getting an object on the top level.
     * @param objectId
     * @return
     */
    public static String getOMIReadObjectMessage(String objectId) {
        ArrayList<String> ids = new ArrayList<>();
        ids.add(objectId);
        return getOMIReadObjectMessage(ids, null);
    }

    public static String getOMIReadObjectMessage(List<String> objectIds, String infoItem) {
        Document doc = new Document(createOMIEnvelope());
        Element omiReadElement = createOMIReadElement(null, null);
        doc.getRootElement().addContent(omiReadElement);
        Element omiMsgElement = createOMIMsgElement();
        omiReadElement.addContent(omiMsgElement);
        Element odfObjectsElement = createODFObjectsElement();
        omiMsgElement.addContent(odfObjectsElement);
        Element parent = odfObjectsElement;
        for (String objectId : objectIds) {
            Element temp = createOdfObjectElement(objectId);
            parent.addContent(temp);
            parent = temp;
        }
        if (infoItem != null) {
            parent.addContent(createInfoItemElement(infoItem));
        }
        return XML_OUTPUTTER.outputString(doc);
    }

    public static String getOMISubscriptionMessage(String portNumber, String patientId, String measurementId) {
        Document doc = new Document(createOMIEnvelope());
        Element omiReadElement = createOMIReadElement(portNumber, "-1");
        doc.getRootElement().addContent(omiReadElement);
        Element omiMsgElement = new Element("msg", "omi", "omi.xsd");
        omiReadElement.addContent(omiMsgElement);
        Element odfObjectsElement = new Element("Objects", "odf.xsd");
        omiMsgElement.addContent(odfObjectsElement);
        Element odfPatientElement = new Element("Object", "odf.xsd");
        odfObjectsElement.addContent(odfPatientElement);
        odfPatientElement.addContent(createOdfIdElement(patientId));
        Element measurementObjectElement = new Element("Object", "odf.xsd");
        odfPatientElement.addContent(measurementObjectElement);
        measurementObjectElement.setAttribute("type", "fhir-observation");
        measurementObjectElement.addContent(createOdfIdElement(measurementId));
        Element infoItemElement = createInfoItemElement("measurement");
        measurementObjectElement.addContent(infoItemElement);
        return XML_OUTPUTTER.outputString(doc);
    }

    public static String getOMICancelMessage(int requestID) {
        Document doc = new Document(createOMIEnvelope());
        Element omiCancelElement = createOMICancelElement(requestID);
        doc.getRootElement().addContent(omiCancelElement);
        return XML_OUTPUTTER.outputString(doc);
    }

    public static String getSubscriptionResponse(String requestId) {
        Document document = new Document();
        document.setRootElement(createOMIEnvelope());
        Element root = document.getRootElement();
        Element responseElement = createResponseElement(requestId);
        root.addContent(responseElement);
        Element resultElement = createResultElement();
        responseElement.addContent(resultElement);
        resultElement.addContent(createOMIReturnElement("200"));
        return XML_OUTPUTTER.outputString(document);
    }

    public static String getMetadataForInfoItem(List<String> objectIds, String infoItem) {
        Document doc = new Document(createOMIEnvelope());
        Element omiReadElement = createOMIReadElement(null, null);
        doc.getRootElement().addContent(omiReadElement);
        Element omiMsgElement = createOMIMsgElement();
        omiReadElement.addContent(omiMsgElement);
        Element odfObjectsElement = createODFObjectsElement();
        omiMsgElement.addContent(odfObjectsElement);
        Element parent = odfObjectsElement;
        for (String objectId : objectIds) {
            Element temp = createOdfObjectElement(objectId);
            parent.addContent(temp);
            parent = temp;
        }
        Element infoItemElement = createInfoItemElement(infoItem);
        parent.addContent(infoItemElement);
        infoItemElement.addContent(new Element("MetaData", "odf.xsd"));
        return XML_OUTPUTTER.outputString(doc);
    }

    private static Element createOMIEnvelope() {
        Element omiEnvelope = new Element("omiEnvelope", "omi", "omi.xsd");
        Namespace omiSpace = Namespace.getNamespace("omi", "omi.xsd");
        Namespace xmlSpace = Namespace.getNamespace("xs", "http://www.w3.org/2001/XMLSchema-instance");
        omiEnvelope.addNamespaceDeclaration(xmlSpace);
        omiEnvelope.addNamespaceDeclaration(omiSpace);

        ArrayList<Attribute> attributes = new ArrayList<Attribute>();
        Attribute version = new Attribute("version", "1.0");
        Attribute ttl = new Attribute("ttl", "0");
        attributes.add(version);
        attributes.add(ttl);
        omiEnvelope.setAttributes(attributes);
        return omiEnvelope;
    }

    private static Element createOMIReadElement(String portNumber, String interval) {
        Element omiReadElement = new Element("read", "omi", "omi.xsd");
        omiReadElement.setAttribute(new Attribute("msgformat", "odf"));
        if (portNumber != null) {
            omiReadElement.setAttribute(new Attribute("callback", "http://localhost:" + portNumber));
        }
        if (interval != null) {
            omiReadElement.setAttribute(new Attribute("interval", interval));
        }
        return omiReadElement;
    }

    private static Element createOMIMsgElement() {
        Element omiMsgElement = new Element("msg", "omi", "omi.xsd");
        return omiMsgElement;
    }

    private static Element createODFObjectsElement() {
        Element objectsElement = new Element("Objects", "odf.xsd");
        return objectsElement;
    }

    private static Element createOMICancelElement(int requestId) {
        Element cancelElement = new Element("cancel", "omi", "omi.xsd");
        Element omiRequestId = new Element("requestID", "omi", "omi.xsd");
        omiRequestId.setText(Integer.toString(requestId));
        cancelElement.addContent(omiRequestId);
        return cancelElement;
    }

    private static Element createResponseElement(String requestId) {
        Element responseElement = new Element("response", "omi", "omi.xsd");
        Element id = new Element("requestID", "omi", "omi.xsd");
        id.setText(requestId);
        responseElement.addContent(id);
        return responseElement;
    }

    private static Element createResultElement() {
        return new Element("result", "omi", "omi.xsd");
    }

    private static Element createOMIReturnElement(String returnCode) {
        Element returnElement = new Element("return", "omi", "omi.xsd");
        returnElement.setAttribute(new Attribute("returnCode", returnCode));
        return returnElement;
    }

    private static Element createOdfObjectElement(String id) {
        Element odfObjectElement = new Element("Object", "odf.xsd");
        odfObjectElement.addContent(createOdfIdElement(id));
        return odfObjectElement;
    }

    private static Element createInfoItemElement(String infoItemName) {
        Element infoItem = new Element("InfoItem", "odf.xsd");
        Attribute nameAttr = new Attribute("name", infoItemName);
        infoItem.setAttribute(nameAttr);
        return infoItem;
    }

    private static Element createOdfIdElement(String id) {
        Element e = new Element("id", "odf.xsd");
        e.setText(id);
        return e;
    }

}
