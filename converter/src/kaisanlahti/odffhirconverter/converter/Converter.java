package kaisanlahti.odffhirconverter.converter;

import kaisanlahti.odffhirconverter.ConnectionHelper;
import kaisanlahti.odffhirconverter.converter.fhir.FHIRMessageFactory;
import kaisanlahti.odffhirconverter.converter.fhir.FHIRParser;
import kaisanlahti.odffhirconverter.converter.odf.ODFMeasurement;
import kaisanlahti.odffhirconverter.converter.odf.ODFPatient;
import kaisanlahti.odffhirconverter.converter.omi.OMIMessageFactory;
import kaisanlahti.odffhirconverter.converter.omi.OMIParser;
import kaisanlahti.odffhirconverter.converter.omi.OMISubscriptionData;

import java.util.ArrayList;

/**
 * Created by jhkaisan on 2016/08/13.
 */
public class Converter {

    private final OMIParser omiParser = new OMIParser();
    private final ConnectionHelper connectionHelper = new ConnectionHelper();

    public ODFPatient readPatientObject(String patientId) {
        ArrayList<String> objectIds = new ArrayList<>();
        objectIds.add(patientId);
        String omiReadObjectsMessage = OMIMessageFactory.getOMIReadObjectMessage(objectIds, null);
        String response = connectionHelper.postOmiMessage(omiReadObjectsMessage);
        ODFPatient patient = omiParser.parsePatientObjectResponse(response);
        if (patient != null) {
            //Check if the patient exists on FHIR server. If not, post it before continuing.
            String fhirPatientGetResponse = new ConnectionHelper().getFhir("Patient", patientId);
            String fhirPatientId = new FHIRParser().parseResourceIdFromSearch("Patient", fhirPatientGetResponse);
            if (fhirPatientId == null) {
                String fhirPatientMessage = FHIRMessageFactory.getFHIRPatient(patient);
                new ConnectionHelper().postFhirMessage(fhirPatientMessage);
            }
        }
        return patient;
    }

    public void sendSubscriptionRequest(OMISubscriptionData data, String portNumber) {
        String omiSubscriptionRequestMessage = OMIMessageFactory.getOMISubscriptionMessage(portNumber, data.getPatient(), data.getInfoItem());
        String response = connectionHelper.postOmiMessage(omiSubscriptionRequestMessage);
        Integer requestId = omiParser.parseSubscriptionResponse(response);
        data.setRequestId(requestId);
    }

    public void sendCancelRequest(OMISubscriptionData data) {
        String omiCancelMessage = OMIMessageFactory.getOMICancelMessage(data.getRequestId());
        String response = connectionHelper.postOmiMessage(omiCancelMessage);
        data.setRequestId(null);
    }
}
