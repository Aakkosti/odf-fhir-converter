package kaisanlahti.odffhirconverter.converter.fhir;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.filter.ElementFilter;
import org.jdom2.input.SAXBuilder;

import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Jaakko on 20.8.2016.
 */
public class FHIRParser {

    private final SAXBuilder builder = new SAXBuilder();

    public String parseResourceIdFromSearch(String resource, String response) {
        String patientId = null;
        try {
            Document document = builder.build(new StringReader(response));
            Iterator<Element> patientIterator = document.getDescendants(new ElementFilter(resource));
            while (patientIterator.hasNext()) {
                Element patientElement = patientIterator.next();
                List<Element> idList = patientElement.getContent(new ElementFilter("id"));
                if (idList.size() > 0) {
                    Element idElement = idList.get(0);
                    patientId = idElement.getAttributeValue("value");
                    if (patientId != null) {
                        break;
                    }
                }
            }
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return patientId;
    }
}
