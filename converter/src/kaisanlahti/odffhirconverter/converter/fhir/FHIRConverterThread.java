package kaisanlahti.odffhirconverter.converter.fhir;

import kaisanlahti.odffhirconverter.ConnectionHelper;
import kaisanlahti.odffhirconverter.converter.omi.OMIMessageFactory;
import kaisanlahti.odffhirconverter.converter.omi.OMIParser;
import kaisanlahti.odffhirconverter.converter.odf.ODFPatient;

import java.io.*;
import java.net.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jhkaisan on 2016/08/13.
 */
public class FHIRConverterThread extends Thread {

    private Socket socket = null;
    private final OMIMessageFactory omiMessageFactory = new OMIMessageFactory();
    private final OMIParser omiParser = new OMIParser();
    private final FHIRParser fhirParser = new FHIRParser();

    public FHIRConverterThread(Socket socket) {
        super("FHIRConverterThread");
        this.socket = socket;
    }

    public void run() {
        ODFPatient patient = null;
        try (
                PrintWriter out =
                        new PrintWriter(this.socket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(this.socket.getInputStream()));
        ) {
            String inputLine, outputLine, requestId;
            String literal = "<omi:requestID>(.*)</omi:requestID>";
            Pattern pattern = Pattern.compile(literal);
            while ((inputLine = in.readLine()) != null) {
                Matcher matcher = pattern.matcher(inputLine);
                while (matcher.find()) {
                    requestId = matcher.group(1);
                    String response = omiMessageFactory.getSubscriptionResponse(requestId);
                    out.write("HTTP/1.1 200 OK\r\n");
                    out.write("Content-Type: text/xml; charset=UTF-8\r\n");
                    out.write("Content-Length: " + response.getBytes("UTF-8").length + "\r\n");
                    out.write("\r\n");
                    out.write(response);
                    out.close();
                    break;
                }
                if (inputLine.contains("<Objects>")) {
                    patient = omiParser.parseSubscriptionMessage(inputLine);
                    break;
                }
            }
            in.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
        if (patient != null) {
            ConnectionHelper helper = new ConnectionHelper();
            String fhirPatientSearchReply = helper.getFhir("Patient", patient.getId());
            FHIRParser parser = new FHIRParser();
            String fhirPatientId = parser.parseResourceIdFromSearch("Patient", fhirPatientSearchReply);
            String message = FHIRMessageFactory.getFHIRObservation(patient.getOdfMeasurements().get(0), fhirPatientId);
            helper.postFhirMessage(message);
        }
    }
}
