package kaisanlahti.odffhirconverter.converter.fhir;

import kaisanlahti.odffhirconverter.converter.odf.ODFDevice;
import kaisanlahti.odffhirconverter.converter.odf.ODFMeasurement;
import kaisanlahti.odffhirconverter.converter.odf.ODFPatient;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by jhkaisan on 2016/08/19.
 */
public class FHIRMessageFactory {

    private static final XMLOutputter XML_OUTPUTTER = getPrettyXMLOutputter();
    private static final String FHIR_XMLNS = "http://hl7.org/fhir";

    private static XMLOutputter getPrettyXMLOutputter() {
        XMLOutputter outputter = new XMLOutputter();
        outputter.setFormat(Format.getPrettyFormat());
        return outputter;
    }

    public static String getFHIRPatient(ODFPatient odfPatient) {
        Element patientElement = new Element("Patient", FHIR_XMLNS);
        Document document = new Document(patientElement);
        patientElement.addContent(getIdentifierElement(odfPatient.getId()));
        patientElement.addContent(getNameElement(odfPatient));
        patientElement.addContent(getBirthDateElement(odfPatient));
        patientElement.addContent(getElementWithValue("gender", odfPatient.getGender().toLowerCase()));
        return XML_OUTPUTTER.outputString(document);
    }

    public static String getFHIRObservation(ODFMeasurement measurement, String fhirPatientId) {
        Element observationElement = new Element("Observation", FHIR_XMLNS);
        Document document = new Document(observationElement);
        observationElement.addContent(getCodeElement(measurement.getId()));
        observationElement.addContent(getElementWithValue("status", "registered"));
        observationElement.addContent(getSubjectElement(fhirPatientId));
        observationElement.addContent(getValueQuantityElement(measurement));
        observationElement.addContent(getDeviceElement(measurement.getDeviceId()));
        return XML_OUTPUTTER.outputString(document);
    }

    public static String getFHIRDevice(ODFDevice device) {
        Element deviceElement = new Element("Device", FHIR_XMLNS);
        Document document = new Document(deviceElement);
        deviceElement.addContent(getIdentifierElement(device.getId()));
        deviceElement.addContent(getElementWithValue("manufacturer", device.getManufacturer()));
        deviceElement.addContent(getElementWithValue("model", device.getModel()));
        deviceElement.addContent(getElementWithValue("lotNumber", device.getSerialNumber()));
        deviceElement.addContent(new Element("type"));

        return XML_OUTPUTTER.outputString(document);
    }

    private static Element getNameElement(ODFPatient odfPatient) {
        Element nameElement = new Element("name", FHIR_XMLNS);
        nameElement.addContent(getUseElement());
        nameElement.addContent(getElementWithValue("given", odfPatient.getGivenName()));
        nameElement.addContent(getElementWithValue("family", odfPatient.getFamilyName()));
        return nameElement;
    }

    private static Element getUseElement() {
        Element useElement = new Element("use", FHIR_XMLNS);
        useElement.setAttribute("value", "official");
        return useElement;
    }

    private static Element getElementWithValue(String elementName, String value) {
        Element element = new Element(elementName, FHIR_XMLNS);
        element.setAttribute("value", value);
        return element;
    }

    private static Element getBirthDateElement(ODFPatient odfPatient) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = df.format(odfPatient.getBirthDate());
        return getElementWithValue("birthDate", dateString);
    }

    private static Element getIdentifierElement(String id) {
        Element element = new Element("identifier", FHIR_XMLNS);
        element.addContent(getElementWithValue("use", "usual"));
        element.addContent(getElementWithValue("value", id));
        return element;
    }

    private static Element getCodeElement(String loincCode) {
        Element codeElement = new Element("code", FHIR_XMLNS);
        Element codingElement = new Element("coding", FHIR_XMLNS);
        codeElement.addContent(codingElement);
        codingElement.addContent(getElementWithValue("system", "http://loinc.org"));
        codingElement.addContent(getElementWithValue("code", loincCode));
        return codeElement;
    }

    private static Element getSubjectElement(String patientId) {
        Element subjectElement = new Element("subject", FHIR_XMLNS);
        subjectElement.addContent(getElementWithValue("reference", "Patient/" + patientId));
        return subjectElement;
    }

    private static Element getValueQuantityElement(ODFMeasurement measurement) {
        Element valueQuantityElement = new Element("valueQuantity", FHIR_XMLNS);
        valueQuantityElement.addContent(getElementWithValue("value", measurement.getValue()));
        if (measurement.getUnit() != null) {
            valueQuantityElement.addContent(getElementWithValue("system", "http://unitsofmeasure.org"));
            valueQuantityElement.addContent(getElementWithValue("unit", measurement.getUnit()));
        }
        return valueQuantityElement;
    }

    private static Element getDeviceElement(String deviceId) {
        Element e = new Element("device", FHIR_XMLNS);
        e.addContent(getElementWithValue("reference", "Device/" + deviceId));
        return e;
    }
}
