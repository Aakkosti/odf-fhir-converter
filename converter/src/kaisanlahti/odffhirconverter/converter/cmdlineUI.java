package kaisanlahti.odffhirconverter.converter;

import kaisanlahti.odffhirconverter.converter.odf.ODFMeasurement;
import kaisanlahti.odffhirconverter.converter.odf.ODFPatient;
import kaisanlahti.odffhirconverter.converter.omi.OMISubscriptionData;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by jhkaisan on 2016/08/13.
 */
public class cmdlineUI {

    private OutputStreamWriter out;
    private BufferedReader in;
    private ArrayList<OMISubscriptionData> subscribedData = new ArrayList<OMISubscriptionData>();
    private ArrayList<OMISubscriptionData> notSubscribedData = new ArrayList<OMISubscriptionData>();
    private final Converter converter;
    private final String portNumber;
    private final String patientId;

    public cmdlineUI(String patientId, String portNumber) {
        try {
            out = new OutputStreamWriter(System.out, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            System.exit(1);
        }

        in = new BufferedReader(new InputStreamReader(
                System.in));
        converter = new Converter();
        this.portNumber = portNumber;
        this.patientId = patientId;
    }


    public void startUI() {
        //Get the patient
        ODFPatient patient = converter.readPatientObject(patientId);
        if (patient == null) {
            System.err.println("Couldn't find the patient in the O-MI server. Aborting.");
            System.exit(1);
        }
        for (ODFMeasurement measurement : patient.getOdfMeasurements()) {
            notSubscribedData.add(new OMISubscriptionData(patient.getId(), measurement.getId()));
        }
        while (true) {
            printOptions();
            try {
                String inputLine = in.readLine();
                int option = Integer.parseInt(inputLine);
                if (option < 1 || option > notSubscribedData.size() + subscribedData.size()) {
                    System.out.println("Invalid index.");
                    continue;
                }
                if (option <= subscribedData.size()) {
                    OMISubscriptionData chosenId = subscribedData.get(option - 1);
                    System.out.printf("Cancelling subscription request for %s %s\n\n", chosenId.getPatient(), chosenId.getInfoItem());
                    converter.sendCancelRequest(chosenId);
                    subscribedData.remove(chosenId);
                    notSubscribedData.add(chosenId);
                } else if (option > subscribedData.size()) {
                    int listIndex = option - 1 - subscribedData.size();
                    OMISubscriptionData data = notSubscribedData.get(listIndex);
                    System.out.printf("Sending subscription request for %s %s\n\n", data.getPatient(), data.getInfoItem());
                    converter.sendSubscriptionRequest(data, portNumber);
                    notSubscribedData.remove(listIndex);
                    subscribedData.add(data);
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }

    private void printOptions() {
        int index = 1;
        if (subscribedData.size() > 0) {
            System.out.println("Objects that we are subscribed to:");
            for (OMISubscriptionData data : subscribedData) {
                System.out.printf("%d. %s: %s with requestID %d\n", index, data.getPatient(), data.getInfoItem(), data.getRequestId());
                index++;
            }
        }
        if (notSubscribedData.size() > 0) {
            System.out.println("Objects that can be subscribed to:");
            for (OMISubscriptionData data : notSubscribedData) {
                System.out.printf("%d. %s: %s\n", index, data.getPatient(), data.getInfoItem());
                index++;
            }
        }
        System.out.println("Type the object number to subscribe or to unsubscribe.");
    }
}
