package kaisanlahti.odffhirconverter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by Jaakko on 20.8.2016.
 */
public class ConnectionHelper {

    private static String omiServer;
    private static String fhirServer;

    public String postOmiMessage(String message) {
        return postMessage(omiServer, message);
    }

    public String postFhirMessage(String message) {
        return postMessage(fhirServer, message);
    }

    public String getFhir(String resource, String odfId) {
        String response = null;
        try {
            URLConnection connection;
            String charset = "UTF-8";
            String format = "xml";
            String query = String.format("identifier=%s&_format=%s",
                    URLEncoder.encode(odfId, charset), URLEncoder.encode(format, charset));
            connection = new URL(fhirServer + "/" + resource + "?" + query).openConnection();
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            connection.setRequestProperty("Content-Type", "application/xml");
            connection.setDoOutput(false);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            connection.getInputStream()));
            String decodedString;
            StringBuilder builder = new StringBuilder();
            while ((decodedString = in.readLine()) != null) {
                builder.append(decodedString);
            }
            in.close();
            response = builder.toString();

        } catch (MalformedURLException e) {
            e.printStackTrace();
            System.err.println("The FHIR server URL is malformed.");
            System.exit(-1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    private String postMessage(String url, String message) {
        String response = null;
        try {
            URLConnection connection;
            if (url.equals(fhirServer)) {
                String charset = "UTF-8";
                String param1 = "xml";
                String query = String.format("_format=%s",
                        URLEncoder.encode(param1, charset));
                connection = new URL(url + "?" + query).openConnection();
            } else {
                connection = new URL(url).openConnection();
            }
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            connection.setRequestProperty("Content-Type", "application/xml");
            connection.setDoOutput(true);
            OutputStreamWriter output = new OutputStreamWriter(connection.getOutputStream());
            output.write(message);
            output.close();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            connection.getInputStream()));
            String decodedString;
            StringBuilder builder = new StringBuilder();
            while ((decodedString = in.readLine()) != null) {
                builder.append(decodedString);
            }
            in.close();
            response = builder.toString();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }


    public static void setOmiServer(String omiServer) {
        ConnectionHelper.omiServer = omiServer;
    }

    public static void setFhirServer(String fhirServer) {
        ConnectionHelper.fhirServer = fhirServer;
    }
}
