package kaisanlahti.devices;

import kaisanlahti.devices.measurement.*;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Provide O-MI server address as the first argument. Optional --debug flag prints" +
                    "the sent O-MI message.");
            System.exit(1);
        }
        String omiServerUrl = args[0];
        boolean debug = false;
        if (args.length == 2 && args[1].equals("--debug")) {
            debug = true;
        }
        readAndPostXmlFile(omiServerUrl, "example-xmls/devices.xml");
        readAndPostXmlFile(omiServerUrl, "example-xmls/patient.xml");
        ArrayList<Measurement> measurements = new ArrayList<>();
        measurements.add(new BloodPressureDiastolicMeasurement());
        measurements.add(new BloodPressureSystolicMeasurement());
        measurements.add(new HeartRateMeasurement());
        measurements.add(new StepMeasurement());
        measurements.add(new EnergyConsumptionMeasurement());
        measurements.add(new WeightMeasurement());
        measurements.add(new SleepMeasurement());
        measurements.add(new BodyFatMeasurement());
        Random random = new Random();
        while (true) {
            Measurement measurement = measurements.get(random.nextInt(measurements.size()));
            System.out.println("Sending update for " + measurement.getId());
            String omiUpdateMessage = measurement.getOMIUpdateMessage();
            if (debug) {
                System.out.println(omiUpdateMessage);
            }
            sendHTTPPostRequest(omiServerUrl, omiUpdateMessage);
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private  static void readAndPostXmlFile(String omiServerUrl, String fileName) {
        try {
            FileInputStream fis = new FileInputStream(fileName);
            BufferedReader in = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = in.readLine()) != null) {
                builder.append(line);
            }
            in.close();
            sendHTTPPostRequest(omiServerUrl, builder.toString());
        } catch (FileNotFoundException e) {
            System.err.println(fileName + " not found.");
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static String sendHTTPPostRequest(String omiServerUrl, String message) {
        String response = "";
        try {
            URLConnection connection = new URL(omiServerUrl).openConnection();
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            connection.setRequestProperty("Content-Type", "application/xml");
            connection.setDoOutput(true);
            OutputStreamWriter output = new OutputStreamWriter(connection.getOutputStream());
            output.write(message);
            output.close();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            connection.getInputStream()));
            String decodedString;
            StringBuilder builder = new StringBuilder();
            while ((decodedString = in.readLine()) != null) {
                builder.append(decodedString);
            }
            in.close();
            response = builder.toString();

        } catch (MalformedURLException e) {
            e.printStackTrace();
            System.err.println("The O-MI server URL is malformed.");
            System.exit(-1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }
}
