package kaisanlahti.devices.measurement;

import kaisanlahti.devices.InfoItem;
import kaisanlahti.devices.InfoItemValue;

import java.util.ArrayList;

/**
 * Created by jhkaisan on 2016/08/19.
 */
public class StepMeasurement extends Measurement {

    public StepMeasurement() {
        super("55423-8");
    }

    protected ArrayList<InfoItem> generateMeasurements() {
        ArrayList<InfoItem> infoItems = new ArrayList<>();
        int steps = random.nextInt(15000);
        InfoItemValue stepsInfoItemValue = new InfoItemValue("xs:integer", Integer.toString(steps), true);
        InfoItem item = new InfoItem("measurement", stepsInfoItemValue);
        infoItems.add(item);
        return infoItems;
    }
}
