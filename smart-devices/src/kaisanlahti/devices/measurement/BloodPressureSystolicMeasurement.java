package kaisanlahti.devices.measurement;

import kaisanlahti.devices.InfoItem;
import kaisanlahti.devices.InfoItemValue;

import java.util.ArrayList;

/**
 * Created by jhkaisan on 2016/08/19.
 */
public class BloodPressureSystolicMeasurement extends Measurement {

    public BloodPressureSystolicMeasurement() {
        super("8480-6");
    }

    protected ArrayList<InfoItem> generateMeasurements() {
        ArrayList<InfoItem> infoItems = new ArrayList<>();
        int systolic = 90 + random.nextInt(70);
        InfoItem bloodPressure = new InfoItem("measurement", new InfoItemValue("xs:integer", Integer.toString(systolic), false));
        infoItems.add(bloodPressure);
        return infoItems;
    }
}
