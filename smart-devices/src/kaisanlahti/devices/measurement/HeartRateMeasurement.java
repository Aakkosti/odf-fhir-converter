package kaisanlahti.devices.measurement;

import kaisanlahti.devices.InfoItem;
import kaisanlahti.devices.InfoItemValue;
import kaisanlahti.devices.OMIMessageFactory;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by jhkaisan on 2016/08/19.
 */
public class HeartRateMeasurement extends Measurement {
    public HeartRateMeasurement() {
        super("8867-4");
    }

    protected ArrayList<InfoItem> generateMeasurements() {
        ArrayList<InfoItem> infoItems = new ArrayList<>();
        int pulse = random.nextInt(150);
        InfoItemValue heartRateInfoItemValue = new InfoItemValue("xs:integer", Integer.toString(pulse), true);
        InfoItem item = new InfoItem("measurement", heartRateInfoItemValue);
        infoItems.add(item);
        return infoItems;
    }
}
