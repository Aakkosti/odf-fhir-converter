package kaisanlahti.devices.measurement;

import kaisanlahti.devices.InfoItem;
import kaisanlahti.devices.InfoItemValue;
import kaisanlahti.devices.OMIMessageFactory;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by jhkaisan on 2016/08/19.
 */
public class WeightMeasurement extends Measurement {

    public WeightMeasurement() {
        super("29463-7");
    }

    protected ArrayList<InfoItem> generateMeasurements() {
        ArrayList<InfoItem> infoItems = new ArrayList<>();
        double weight = 80.0 + random.nextDouble() * 10.0;
        InfoItemValue weightValue = new InfoItemValue("xs:double", Double.toString(weight).substring(0, 4), true);
        InfoItem weightInfoItem = new InfoItem("measurement", weightValue);
        infoItems.add(weightInfoItem);
        return infoItems;
    }

}
