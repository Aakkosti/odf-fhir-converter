package kaisanlahti.devices.measurement;

import kaisanlahti.devices.InfoItem;
import kaisanlahti.devices.InfoItemValue;

import java.util.ArrayList;

/**
 * Created by jhkaisan on 2016/08/19.
 */
public class SleepMeasurement extends Measurement {

    public SleepMeasurement() {
        super("Sleep");
    }

    @Override
    protected ArrayList<InfoItem> generateMeasurements() {
        ArrayList<InfoItem> infoItems = new ArrayList<>();
        int sleepMinutes = random.nextInt(540);
        InfoItemValue sleepMinutesInfoItemValue = new InfoItemValue("xs:integer", Integer.toString(sleepMinutes), true);
        InfoItem sleepInfo = new InfoItem("measurement", sleepMinutesInfoItemValue);
        infoItems.add(sleepInfo);
        return infoItems;
    }
}
