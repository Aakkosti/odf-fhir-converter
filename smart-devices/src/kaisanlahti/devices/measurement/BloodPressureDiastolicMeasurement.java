package kaisanlahti.devices.measurement;

import kaisanlahti.devices.InfoItem;
import kaisanlahti.devices.InfoItemValue;

import java.util.ArrayList;

/**
 * Created by jhkaisan on 2016/08/19.
 */
public class BloodPressureDiastolicMeasurement extends Measurement {
    public BloodPressureDiastolicMeasurement() {
        super("8462-4");
    }

    protected ArrayList<InfoItem> generateMeasurements() {
        ArrayList<InfoItem> infoItems = new ArrayList<>();
        int diastolic = 60 + random.nextInt(40);
        InfoItem bloodPressure = new InfoItem("measurement", new InfoItemValue("xs:integer", Integer.toString(diastolic), false));
        infoItems.add(bloodPressure);
        return infoItems;
    }
}
