package kaisanlahti.devices.measurement;

import kaisanlahti.devices.InfoItem;
import kaisanlahti.devices.InfoItemValue;
import kaisanlahti.devices.OMIMessageFactory;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by jhkaisan on 2016/08/19.
 */
public class BodyFatMeasurement extends Measurement {
    public BodyFatMeasurement() {
        super("41982-0");
    }

    protected ArrayList<InfoItem> generateMeasurements() {
        ArrayList<InfoItem> infoItems = new ArrayList<>();
        double bodyfat = 15.0 + random.nextDouble() * 10.0;
        InfoItemValue bodyfatValue = new InfoItemValue("xs:double", Double.toString(bodyfat).substring(0, 4), true);
        InfoItem bodyFatInfoItem = new InfoItem("measurement", bodyfatValue);
        infoItems.add(bodyFatInfoItem);
        return infoItems;
    }
}
