package kaisanlahti.devices.measurement;

import kaisanlahti.devices.InfoItem;
import kaisanlahti.devices.InfoItemValue;

import java.util.ArrayList;

/**
 * Created by jhkaisan on 2016/08/19.
 */
public class EnergyConsumptionMeasurement extends Measurement {

    public EnergyConsumptionMeasurement() {
        super("Calories");
    }

    protected ArrayList<InfoItem> generateMeasurements() {
        ArrayList<InfoItem> infoItems = new ArrayList<>();
        int calories = 1500 + random.nextInt(1000);
        InfoItemValue caloriesInfoItemValue = new InfoItemValue("xs:integer", Integer.toString(calories), true);
        InfoItem caloriesItem = new InfoItem("measurement", caloriesInfoItemValue);
        infoItems.add(caloriesItem);
        return infoItems;
    }
}
