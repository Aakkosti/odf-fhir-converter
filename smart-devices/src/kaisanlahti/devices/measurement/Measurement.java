package kaisanlahti.devices.measurement;

import kaisanlahti.devices.InfoItem;
import kaisanlahti.devices.OMIMessageFactory;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by jhkaisan on 2016/08/19.
 */
public abstract class Measurement {

    private final String id;
    protected final Random random = new Random();
    private ArrayList<InfoItem> infoItems;

    public Measurement(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public ArrayList<InfoItem> getInfoItems() {
        return infoItems;
    }

    public String getOMIUpdateMessage() {
        infoItems = new ArrayList<>();
        infoItems.addAll(generateMeasurements());
        return OMIMessageFactory.createOMIWriteMessage(this);
    }

    protected abstract ArrayList<InfoItem> generateMeasurements();
}
