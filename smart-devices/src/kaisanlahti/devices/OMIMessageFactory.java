package kaisanlahti.devices;

import kaisanlahti.devices.measurement.Measurement;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.util.ArrayList;

/**
 * Created by jhkaisan on 2016/08/18.
 */
public class OMIMessageFactory {

    private static final XMLOutputter xmlOutput = getXMLOutputter();

    public static String createOMIWriteMessage(Measurement measurement) {
        Document document = new Document(createOMIEnvelope());
        Element omiWriteElement = createOMIWriteElement();
        document.getRootElement().addContent(omiWriteElement);
        Element omiMsgElement = createOMIMsgElement();
        omiWriteElement.addContent(omiMsgElement);
        Element odfObjectsElement = createODFObjectsElement();
        omiMsgElement.addContent(odfObjectsElement);
        Element odfObjectPatientElement = createODFObjectElement("patient");
        odfObjectsElement.addContent(odfObjectPatientElement);
        odfObjectPatientElement.addContent(createOdfIdElement("Paavo1"));
        Element odfObjectObservationElement = createODFObjectElement("fhir-observation");
        odfObjectPatientElement.addContent(odfObjectObservationElement);
        odfObjectObservationElement.addContent(createOdfIdElement(measurement.getId()));
        addInfoItems(odfObjectObservationElement, measurement.getInfoItems());
        return xmlOutput.outputString(document);
    }

    private static XMLOutputter getXMLOutputter() {
        XMLOutputter output = new XMLOutputter();
        output.setFormat(Format.getPrettyFormat());
        return output;
    }

    private static Element createOMIEnvelope() {
        Element omiEnvelope = new Element("omiEnvelope", "omi", "omi.xsd");
        Namespace omiSpace = Namespace.getNamespace("omi", "omi.xsd");
        Namespace xmlSpace = Namespace.getNamespace("xs", "http://www.w3.org/2001/XMLSchema-instance");
        omiEnvelope.addNamespaceDeclaration(xmlSpace);
        omiEnvelope.addNamespaceDeclaration(omiSpace);

        ArrayList<Attribute> attributes = new ArrayList<>();
        Attribute version = new Attribute("version", "1.0");
        Attribute ttl = new Attribute("ttl", "0");
        attributes.add(version);
        attributes.add(ttl);
        omiEnvelope.setAttributes(attributes);
        return omiEnvelope;
    }

    private static Element createOMIWriteElement() {
        Element omiWrite = new Element("write", "omi", "omi.xsd");
        Attribute msgFormatAttribute = new Attribute("msgformat", "odf");
        omiWrite.setAttribute(msgFormatAttribute);
        return omiWrite;
    }

    private static Element createOMIMsgElement() {
        Element omiMsg = new Element("msg", "omi", "omi.xsd");
        return omiMsg;
    }

    private static Element createODFObjectsElement() {
        Element odfObjects = new Element("Objects", "odf.xsd");
        return odfObjects;
    }

    private static Element createODFObjectElement(String type) {
        Element odfObject = new Element("Object", "odf.xsd");
        Attribute typeAttribute  = new Attribute("type", type);
        odfObject.setAttribute(typeAttribute);
        return odfObject;
    }

    private static void addInfoItems(Element e, ArrayList<InfoItem> infoItems) {
        for (InfoItem item : infoItems) {
            Element infoItemElement = new Element("InfoItem", "odf.xsd");
            Attribute nameAttribute = new Attribute("name", item.getName());
            infoItemElement.setAttribute(nameAttribute);
            for (InfoItemValue value : item.getValues()) {
                infoItemElement.addContent(createOdfValueElement(value));
            }
            addMetaDataElement(infoItemElement, item.getMetaData());
            e.addContent(infoItemElement);
        }
    }

    private static Element createOdfValueElement(InfoItemValue value) {
        Element valueElement = new Element("value", "odf.xsd");
        valueElement.setText(value.getValue());
        Attribute typeAttribute = new Attribute("type", value.getType());
        valueElement.setAttribute(typeAttribute);
        if (value.getUnixTimestamp() != null) {
            Attribute unixTimeStampAttribute = new Attribute("unixTime", value.getUnixTimestamp());
            valueElement.setAttribute(unixTimeStampAttribute);
        }
        if (value.getHumanReadableDate() != null) {
            Attribute dateTimeAttribute = new Attribute("dateTime", value.getHumanReadableDate());
            valueElement.setAttribute(dateTimeAttribute);
        }
        return  valueElement;
    }

    private static Element createOdfIdElement(String id) {
        Element idElement = new Element("id", "odf.xsd");
        idElement.setText(id);
        return idElement;
    }

    private static void addMetaDataElement(Element e, ArrayList<InfoItem> metaData) {
        if (metaData.size() > 0) {
            Element metadataElement = new Element("MetaData", "odf.xsd");
            addInfoItems(metadataElement, metaData);
            e.addContent(metadataElement);
        }
    }
}
