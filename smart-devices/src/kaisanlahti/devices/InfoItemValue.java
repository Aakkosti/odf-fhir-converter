package kaisanlahti.devices;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jhkaisan on 2016/08/18.
 */
public class InfoItemValue {

    private final String type;
    private final String value;
    private String unixTimestamp;
    private String humanReadableDate;

    public InfoItemValue(String type, String value, boolean generateTimestamps) {
        this.type = type;
        this.value = value;
        if (generateTimestamps) {
            this.unixTimestamp = Long.toString(System.currentTimeMillis() / 1000L);
            this.humanReadableDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SXXX").format(new Date());
        }
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public String getUnixTimestamp() {
        return unixTimestamp;
    }

    public String getHumanReadableDate() {
        return humanReadableDate;
    }
}
