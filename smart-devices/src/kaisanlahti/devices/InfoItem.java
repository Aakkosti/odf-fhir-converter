package kaisanlahti.devices;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by jhkaisan on 2016/08/18.
 */
public class InfoItem {

    private final String name;
    private final ArrayList<InfoItemValue> values = new ArrayList<>();
    private final ArrayList<InfoItem> metaData = new ArrayList<>();

    public InfoItem(String name, InfoItemValue... values) {
        this.name = name;
        this.values.addAll(Arrays.asList(values));
    }

    public String getName() {
        return name;
    }

    public ArrayList<InfoItemValue> getValues() {
        return values;
    }

    public ArrayList<InfoItem> getMetaData() {
        return metaData;
    }
}
