#Smart devices
A program to generate updates from
simulated smart devices every 10 seconds.

To run: `java -jar smart-devices.jar [url to O-MI-server]`.

The jar reads `example-xmls/patient.xml` and 
`example-xmls/devices.xml` on startup,
so make sure you have the files in that path in your working directory.
The patient XML file also has some hardcoded URLs to ODF objects,
so you need to change those if your server isn't at
`http://localhost:8080`.

Optional flag `--debug` prints the O-MI message that
is sent to the server.